# StateCharts

## Consideraciones generales

Los eventos son estados, operaciones o constantes.

### Tipos de fenómenos

Existen 3 tipos de fenómenos que son relevantes al construir una especificación

- Clase 1) EC - U
- Clase 2) EC - S
- Clase 3) MC - S

La 4ta clase (MC - U) es solo relevante para la etapa de programación.

En el DK intervienen fenómenos de las clases 1 y 2 (solamente controlados por el entorno) y en S intervienen fenómenos de las clases 2 y 3 (solamente compartidos).

### Cuándo R (formalizado) no es S

- Hay fenómenos no compartidos (R predica sobre fenomenos no compartidos)
- Hay referencias a futuro (e.g. última vez que ocurre un evento x)
- El sistema debe restringir un evento controlado por el entorno

## Ejemplo - Cruce ferroviario

### Designaciones

La distancia de seguridad para bajar la barrera &#8776; X

La distancia de seguridad para subir la barrera &#8776; Y

Un tren cruza la distancia de seguridad acercándose al cruce por la por la via v &#8776; TrenX(v)

Un tren alcanza la zona de la carretera por la via v &#8776; TrenC(v)

Un tren se detiene en la zona de peligro por la via v &#8776; TrenD(v)

El último vagón de un tren para la distancia Y alejándose del cruce por la via v &#8776; TrenY(v)

El sistema enciende la alarma &#8776; AOn

El sistema apaga la alarma &#8776; AOff

El sistema baja la barrera b &#8776; B(b)

El sistema sube la barrera b &#8776; S(b)

El sistema pone el semáforo s en rojo &#8776; R(s)

El sistema pone el semáforo s en verde &#8776; V(s)

El sensor a distancia X de la via v envía una señal cuando el tren se acerca al cruce &#8776; XSens(v)

El sensor a distancia Y de la via v envía una señal cuando el tren se aleja del cruce &#8776; YSens(v)

El sensor a distancia Z de la via v envía una señal cuando el tren se acerca al cruce &#8776; ZSens(v)

Tiempo de espera desde la última señal YSens &#8776; ty

Tiempo de espera desde la última señal ZSens &#8776; td

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
X | EC | U
Y | EC | U
TrenX | EC | U
TrenY | EC | U
TrenC | EC | U
TrenD | EC | U
AOn | MC | S
AOff | MC | S
B | MC | S
S | MC | S
V | MC | S
R | MC | S
XSens | EC | S
YSens | EC | S
ZSens | EC | S
ty | EC | S
td | EC | S

## Ejemplo - Brazo, cinta transportadora y prensa

### Designaciones

El sensor de la cinta transportadora detecta una pieza &#8776; p

El brazo del robot toma una pieza &#8776; take

El brazo del robot comieza a moverse hacia la prensa &#8776; topress

El brazo del robot suelta la pieza &#8776; release

El brazo del robot comieza a moverse hacia la cinta &#8776; tobelt

La prensa comienza a prensar &#8776; press

Se limpia el lugar de prensado &#8776; remove

El prensado terminó &#8776; stoppress

La prensa está libre para volver a prensar &#8776; free

Se enciende la celda de producción &#8776; start

Se apaga la celda de producción &#8776; abort

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
p | EC | S
take | MC | S
topress | MC | S
release | MC | S
tobelt | MC | S
press | MC | S
remove | MC | S
stoppress | EC | S
free | EC | S
start | EC | S
abort | EC | S

## Practica - Ejercicio 1

### Designaciones

Se presiona el botón b1 &#8776; pb1

Se presiona el botón b2 &#8776; pb2

Se prende la lámpara &#8776; LOn

Se apaga la lámpara &#8776; LOff

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
pb1 | EC | S
pb2 | EC | S
LOn | MC | S
LOff | MC | S

## Practica - Ejercicio 2

### Designaciones

Al menos una persona está parada en la zona de influencia interior &#8776; PInt

Al menos una persona está parada en la zona de influencia exterior &#8776; PExt

Ninguna persona está parada en la zona de influencia interior o exterior &#8776; PEmpty

El sensor detecta al menos una persona parada en la zona de influencia interior &#8776; SensInt

El sensor detecta al menos una persona parada en la zona de influencia exterior &#8776; SensExt

Se abre la puerta &#8776; Open

Se cierra la puerta &#8776; Close

Tiempo que permanecen las puertas abiertas si no se detectan personas en la zona de influencia &#8776; tWait

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
PInt | EC | U
PExt | EC | U
PEmpty | EC | U
SensInt | EC | S
SensExt | EC | S
Open | MC | S
Close | MC | S
tWait | EC | S

## Practica - Ejercicio 3

### Designaciones

Se pulsa el botón izquierdo &#8776; PIzq

Se suelta el botón izquierdo &#8776; SIzq

Se pulsa el botón derecho &#8776; PDer

Se suelta el botón derecho &#8776; SDer

Se pulsa el botón central &#8776; PCen

Se suelta el botón central &#8776; SCen

Se arrastra el mouse &#8776; PArr

Se clickea un objeto de la pantalla con el botón derecho &#8776; DClick

Se clickea un objeto de la pantalla con el botón izquierdo &#8776; IClick

Se hace un doble click en un objeto de la pantalla &#8776; Click2

Se arrastra un objeto de la pantalla con el botón derecho &#8776; DDrag

Se arrastra un objeto de la pantalla con el botón izquierdo &#8776; IDrag

Tiempo máximo entre 2 pulzaciones seguidas de un botón para que se considere un doble click &#8776; t

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
PIzq | EC | S
SIzq | EC | S
PDer | EC | S
SDer | EC | S
PSCen | EC | S
PArr | EC | S
t | EC | S
DClick | MC | S
IClick | MC | S
Click2 | MC | S
DDrag | MC | S
IDrag | MC | S

## Practica - Ejercicio 4

### Designaciones

Se consulta el sensor de temperatura s &#8776; ConsTemp

Se consulta el sensor de presion s &#8776; ConsPres

Se consulta el sensor de pulso s &#8776; ConsPuls

Se detecta lectura en rango para sensor de temperatura s &#8776; TempOk

Se detecta lectura en rango para sensor de presion s &#8776; PresOk

Se detecta lectura en rango para sensor de pulso s &#8776; PulsOk

El paciente p tiene una temperatura fuera de rango &#8776; FueraRangoTemp(p)

El paciente p tiene presion fuera de rango  &#8776; FueraRangoPres(p)

El paciente p tiene pulso temperatura fuera de rango  &#8776; FueraRangoPuls(p)

Se detecta lectura fuera de rango para sensor de temperatura s y se reporta al monitor &#8776; FRTemp(s)

Se detecta lectura fuera de rango para sensor de presion s y se reporta al monitor &#8776; FRPres(s)

Se detecta lectura fuera de rango para sensor de pulso s y se reporta al monitor &#8776; FRPuls(s)

No se registran medidas de temperatura hace t segundos para s y se reporta al monitor &#8776; SinRegistroTemp(s)

No se registran medidas de presion hace t segundos para s y se reporta al monitor &#8776; SinRegistroPres(s)

No se registran medidas de pulso hace t segundos para s y se reporta al monitor &#8776; SinRegistroCons(s)

Frecuencia en segundos con la que se toman mediciones &#8776; frec

Tiempo máximo que tarda un sensor en devolver respuesta + 1 &#8776; t

Indica cuando se debe tomar una medición &#8776; Sensar

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
ValorTemp | EC | S
ValorPres | EC | S
ValorPuls | EC | S
TempOk | EC | S
PresOk | EC | S
PulsOk | EC | S
frec | EC | S
t | EC | S
ConsTemp | MC | S
ConsPres | MC | S
ConsPuls | MC | S
FRTemp | MC | S
FRPres | MC | S
FRPuls | MC | S
FueraRangoTemp | EC | S
FueraRangoPres | EC | S
FueraRangoPuls | EC | S
SinRegistroTemp | MC | S
SinRegistroPres | MC | S
SinRegistroPuls | MC | S
Sensar | MC | S

## Practica - Ejercicio 5

### Designaciones

Se apreta el botón s &#8776; Presiona(s)

Se cambia el semáforo s a amarillo &#8776; Amarillo(s)

Se cambia el semáforo s a rojo &#8776; Rojo(s)

Se cambia el semáforo s a verde &#8776; Verde(s)

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
Presiona | EC | S
Amarillo | MC | S
Rojo | MC | S
Verde | MC | S

## Practica - Ejercicio 6

### Designaciones

El sensor de la cinta transportadora s detecta una pieza &#8776; p(s)

El sensor de la cinta transportadora s detecta dos piezas seguidas demasiado juntas &#8776; pjuntas(s)

La máquina indica que en la cinta transportadora s hay una pieza &#8776; piece(s)

El brazo del robot toma una pieza de la cinta más cercana y la deposita donde corresponde &#8776; take

Detener la marcha de la cinta s &#8776; pause(s)

Reanudar la marcha de la cinta s &#8776; resume(s)

El brazo del robot comieza a moverse hacia la cinta s &#8776; tobelt(s)

Se indica que se encienda la celda de producción &#8776; start

Se indica que se encienda la celda de producción &#8776; abort

Se enciende la celda de producción &#8776; init

Se apaga la celda de producción &#8776; end

Tiempo en el que tarda el brazo en tomar un objeto, soltarlo en donde corresponde y volver a la cinta de donde lo tomó &#8776; t

Señal que indica que se debe tomar una pieza de una cinta s &#8776; fetch(s)

Señal que indica que ya se tomó una pieza de la cinta s &#8776; done(s)

Señal que indica que se detectaron 2 piezas demasiado juntas en la cinta s &#8776; danger(s)

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
p | EC | S
pjuntas | EC | S
start | EC | S
abort | EC | S
t | EC | S
take | MC | S
pause | MC | S
resume | MC | S
tobelt | MC | S
piece | MC | S
init | MC | S
end | MC | S
fetch | MC | S
done | MC | S
danger | MC | S

## Practica - Ejercicio 7

### Designaciones

El sensor de la cinta transportadora s detecta una pieza &#8776; p(s)

La máquina indica que en la cinta transportadora s hay una pieza &#8776; piece(s)

El brazo del robot toma una pieza de la cinta más cercana &#8776; take

Detener la marcha de la cinta s &#8776; pause(s)

Reanudar la marcha de la cinta s &#8776; resume(s)

El brazo del robot comieza a moverse hacia la cinta s &#8776; tobelt(s)

Se indica que se encienda la celda de producción &#8776; start

Se indica que se encienda la celda de producción &#8776; abort

Se enciende la celda de producción &#8776; init

Se apaga la celda de producción &#8776; end

Tiempo en el que tarda el brazo en tomar un objeto, soltarlo en donde corresponde y volver a la cinta de donde lo tomó &#8776; t

Tiempo mínimo para el cual se considera que dos piezas no están juntas &#8776; tpieces

Señal que indica que se debe tomar una pieza de una cinta s &#8776; fetch(s)

Señal que indica que ya se tomó una pieza de la cinta s &#8776; done(s)

Señal que indica que se detectaron 2 piezas demasiado juntas en la cinta s &#8776; danger(s)

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
p | EC | S
start | EC | S
abort | EC | S
t | EC | S
tpieces | EC | S
take | MC | S
pause | MC | S
resume | MC | S
tobelt | MC | S
piece | MC | S
init | MC | S
end | MC | S
fetch | MC | S
done | MC | S
danger | MC | S

## Practica - Ejercicio 8

Suposiciones:
- Cada obra de arte tiene una sola persiana y una sola lámpara que incide en la iluminación resultante.
- la cantidad adecuada de luz se puede alcanzar con los rangos de las varillas, persianas y lámparas. Es decir, en S_Varillas no se puede dar SensMenor [IN VMin].
- la cantidad adecuada de luz se puede alcanzar con incrementos de unidad de varillas, persianas o lámparas.

Como el estado inicial es de mínima luminosidad, se van abriendo las varillas, elevando las persianas y prendiendo las luces hasta que el sensor devuelva Ok. Por las suposiciones anteriores, eventualmente ocurrirá al incrementar de a una unidad cada fuente de luz.

### Designaciones

El sensor s indica que la luminosidad es correcta &#8776; SensOk(s)

El sensor s indica que la luminosidad está por debajo de la ideal &#8776; SensMenor(s)

El sensor s indica que la luminosidad está por encima de la ideal &#8776; SensMayor(s)

La varilla v se abre un grado con respecto a su anterior posición &#8776; OpenV(v)

La varilla v se cierra un grado con respecto a su anterior posición &#8776; CloseV(v)

La varilla v no se puede abrir más &#8776; VLimitSup(v)

La varilla v no se puede cerrar más &#8776; VLimitInf(v)

La persiana p se eleva un centímetro con respecto a su anterior posición &#8776; OpenP(p)

La persiana p baja un centímetro con respecto a su anterior posición &#8776; CloseP(p)

La persiana p no se puede elevar más &#8776; PLimitSup(p)

La persiana p no se puede bajar más &#8776; PLimitInf(p)

Se aumenta la intensisad de la lámpara l &#8776; BoostL(l)

Se disminuye la intensisad de la lámpara l &#8776; NerfL(l)

La intensidad de la lámpara l no se puede aumentar más &#8776; LLimitSup(l)

La intensidad de la lámpara l no se puede disminuir más &#8776; LLimitInf(l)

La varilla v está abierta al máximo, se debe comenzar a abrir la persiana v &#8776; MaxV(v)

La persiana p está elevada al máximo, se debe comenzar a aumentar la intensidad de la lampara p &#8776; MaxP(p)

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
SensOk | EC | S
SensMenor | EC | S
SensMayor | EC | S
OpenV | MC | S
CloseV | MC | S
VLimitSup | EC | S
VLimitInf | EC | S
MaxV | MC | S
OpenP | MC | S
CloseP | MC | S
PLimitSup | EC | S
PLimitInf | EC | S
MaxP | MC | S
OpenL | MC | S
CloseL | MC | S
LLimitSup | EC | S
LLimitInf | EC | S

## Practica - Ejercicio 9

### Designaciones

La llave se apaga &#8776; KApagado

Señal que indica que la llave se apaga &#8776; KA

La llave cambia a estado contacto &#8776; KContacto

Señal que indica que la llave cambia a estado contacto &#8776; KC

La llave cambia a estado encendido &#8776; KEncendido

Señal que indica que la llave cambia a estado encendido &#8776; KE

El auto está frenado &#8776; PuntoMuerto

El auto se mueve entre 0.1 y 30km/h &#8776; Marcha1

El auto se mueve entre 30.1 y 50km/h &#8776; Marcha2

El auto se mueve entre 50.1 y 70km/h &#8776; Marcha3

El auto se mueve superando los 70km/h &#8776; Marcha4

Se mueve la perilla a la posición de luces apagadas &#8776; PApagadas

Señal que indica que las lueces se apagan &#8776; LA

Se mueve la perilla a la posición de luces de posición &#8776; PPosicion

Señal que indica que las lueces se ponen en modo posición &#8776; LP

Se mueve la perilla a la posición de luces cortas &#8776; PCorta

Señal que indica que las lueces se ponen en modo corto &#8776; LC

Se indica que se quieren subir los vidrios &#8776; SubirVidrios

Se indica que se quieren bajar los vidrios &#8776; BajarVidrios

Señal que activa subir los vidrios &#8776; SV

Señal que activa bajar los vidrios &#8776; BV

Se pueden subir y bajar los vidrios &#8776; SiV

No se pueden subir y bajar los vidrios &#8776; NoV

Se pueden conducir &#8776; SiC

No se pueden conducir &#8776; NoC

### Tabla de control y visibilidad

Fenómeno | Control | Visibilidad
--- | --- | ---
KApagado | EC | S
KContacto | EC | S
KEncendido | EC | S
KA | MC | S
KC | MC | S
KE | MC | S
PuntoMuerto | MC | S
Marcha1 | MC | S
Marcha2 | MC | S
Marcha3 | MC | S
Marcha4 | MC | S
PApagadas | EC | S
PPosicion | EC | S
PCortas | EC | S
LA | MC | S
LP | MC | S
LC | MC | S
SubirVidrios | EC | S
BajarVidrios | EC | S
SV | MC | S
BV | MC | S
SiV | MC | S
NoV | MC | S
SiC | MC | S
NoC | MC | S
